﻿<%@ Page Title="Fotografo" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Fotografo.aspx.cs" Inherits="Fotografo" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Imgrese Um Fotografo.</h2>
    <h3>Your contact page.</h3>
    <div class="col-md-4">
                <label class="form-label">Ingrese el nombre del fotografo:</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="tbnombre"></asp:TextBox>
            </div>
    <div class="col-md-4">
                <label class="form-label">Ingrese el apellido del fotografo:</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="TextBox1"></asp:TextBox>
            </div>
    <div class="col-md-4">
                <label class="form-label">Ingrese la cedula del fotografo:</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="TextBox2"></asp:TextBox>
            </div>
    <div class="col-md-4">
                <label class="form-label">Ingrese el telefono:</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="TextBox3"></asp:TextBox>
            </div>
    <div class="col-md-4">
                <label class="form-label">Ingrese la direccion:</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="TextBox4"></asp:TextBox>
            </div>
    <div class="col-md-4">
                <label class="form-label">Ingrese el email::</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="TextBox5"></asp:TextBox>
            </div>

    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button" class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="" class="btn btn-success form-control-sm">
              Cancelar
            </a>
        </div>
    </div>

</asp:Content>

