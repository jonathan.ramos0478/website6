﻿<%@ Page Title="Orden" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Orden.aspx.cs" Inherits="Orden" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Ordemes de Travajo</h1>
    <h3>Ingrese su Orden de Trabajo.</h3>
    <div class="col-md-4">
                <label class="form-label">Ingrese el nombre de la orden:</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="tbnombre"></asp:TextBox>
            </div>
    <div class="col-md-4">
                <label class="form-label">Ingrese descripcion de la orden:</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="TextBox1"></asp:TextBox>
            </div>
    <div class="col-md-4">
                <label class="form-label">Ingrese precio de la orden:</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="TextBox2"></asp:TextBox>
            </div>
    <div class="col-md-4">
                <label class="form-label">Ingrese estado de la orden:</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="TextBox3"></asp:TextBox>
            </div>
    <div class="col-md-4">
                <label class="form-label">Ingrese fecha de inicio:</label>
                <asp:TextBox runat="server" TextMode="Date" CssClass="form-control" ID="TextBox4"></asp:TextBox>
            </div>
    <div class="col-md-4">
                <label class="form-label">Ingrese fecha de caducidad:</label>
                <asp:TextBox runat="server" TextMode="Date" CssClass="form-control" ID="TextBox5"></asp:TextBox>
            </div>


    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button" class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="" class="btn btn-success form-control-sm">
              Cancelar
            </a>
        </div>
    </div>

</asp:Content>

