﻿<%@ Page Title="Pedid" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Pedid.aspx.cs" Inherits="Pedid" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Pedidos</h1>
    <h3>Tu pagima de pedidos.</h3>
    <div class="col-md-4">
                <label class="form-label">Ingrese el nombre del Pedido:</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="tbnombre"></asp:TextBox>
            </div>
    <div class="col-md-4">
                <label class="form-label">Ingrese descripcion del Pedido:</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="TextBox1"></asp:TextBox>
            </div>
    <div class="col-md-4">
                <label class="form-label">Ingrese estado del pedido:</label>
                <asp:TextBox runat="server" CssClass="form-control" ID="TextBox2"></asp:TextBox>
            </div>
    <div class="col-md-4">
                <label class="form-label">Ingrese la fecha del pedido:</label>
                <asp:TextBox runat="server" TextMode="Date" CssClass="form-control" ID="TextBox3"></asp:TextBox>
            </div>

    <div class="row">
        <div class="col-md-12 text-center">
            <button type="submit" name="button" class="btn btn-primary">
              Guardar
            </button>
            &nbsp;
            <a href="" class="btn btn-success form-control-sm">
              Cancelar
            </a>
        </div>
    </div>

</asp:Content>
